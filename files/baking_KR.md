# Baking

# 베이킹은 무엇인가요? {#what_KR}

베이킹은 Tezos 블록체인에서 블록을 생성하고 블록을 입증하는 방법입니다. 블록을 만들기 위해서는(To bake block) 최소 10,000 XTZ (1롤)가 필요한 [Tezos Proof-of-stake](proofofstake_KR.md#consensus_KR) 시스템에 참여해야합니다. 소유 한 XTZ가 많을수록 블록을 만들고(Baking) 블록 생성 보상(baking reward)을 얻을 확률이 높아집니다.

# 위임(delegating)은 무엇인가요? {#delegate}

누군가 (혼자서 베이킹 하기에 충분한 )XTZ가 없거나 블록을 생성할(to bake) 컴퓨팅 인프라를 만들고 싶지 않다면, 코인을 베이커 (일명 "위임자", delegator)에게 위임 할 수 있습니다. 위임은 코인 보유자 (즉, 위임인)는 동전을 베이커 (즉, "대표자")에게 "빌려주고", 베이커에게 블록을 만들고(to bake) 블록을 보증하기 위해(endorse) 선택 될 가능성을 높입니다. 실제로, 베이커는 대개 위임 받은 토큰에서 발생한 추가 수익을 코인 보유자와 나눕니다. 중요한 것은 이 과정이 실제로 코인의 소유권(ownership)을 주지 않기 때문에 위임 된 XTZ를 쓰거나 통제 할 수 없고 베이커가 위임자의 자금을 도용하지 못하게 보장합니다.

# 반드시 베이킹을 하거나 위임을 해야하나요? {#bakeordelegate_KR}

셀프 베이킹(Self-baking)을 통해 베이커는 높은 수익을 얻을 수 있지만 (셀프 베이킹을 하기 위해서)베이커를 세팅하고 유휴 시간 없이 안정적으로 베이킹 소프트웨어를 실행할 전문적인 기술지식과 시간이 필요합니다. Tezos 토큰을 위임함으로써 토큰 보유자는 이 프로세스를 완전히 생략 할 수 있지만 일반적으로 낮은 수익을 얻습니다. 현재 프로토콜에서 XTZ가 10,000 개 미만인 토큰 소유자는 다른 베이커에게 위임하여 베이킹에 참여할 수 있습니다.

[이 페이지에서 ](https://mytezosbaker.com/)베이커 리스트를 찾을 수 있으며 알려진 Tezos 대표자(delegates)를 비교 해 볼 수 있습니다.

# 베이킹이나 위임을 통해서 얼마나 이익을 얻을 수 있나요? {#earn_KR}

현재의 Tezos 프로토콜은 첫 해에 약 5.51 %의 토큰 공급을 증가시킵니다 (블록당 16개의 XTZ 보상 및 보증당 2개의 XTZ 보상 기준).

즉 **모든** Tezos 토큰 소지자가 모든 토큰 (예: 전체 Tezos coin 공급량)을 베이킹하면 베이킹 보상은 연간 약 5.51%에 가깝습니다. 그러나 시간 선호도에 주어진 변화량과 (Tezos에 대한)지식 및 (베이커)기능에 따라서 모든 토큰 소지자가 베이킹을 할것 같지는 않아서 베이킹에 대한 기대 수익률은 실제로 연간 5.51 % 이상입니다 . 예를 들어, Tezos 토큰 공급의 50%가 예치되어 있다면 베이킹 보상은 인플레이션율의 두 배인 11%에 가깝게됩니다.

실제로 토큰을 위임한 토큰 소유자의 상은 직접 베이킹을 하는 것보다 적습니다. 왜냐하면 대표자가 베이킹 보상을 수수료를 뺀 나머지를 대리인과 나누기 때문입니다. 이 수수료는 5%에서 20%이며([링크](https://mytezosbaker.com/) 참조) 베이커에 따라 다릅니다.

# 고유 계좌 (implicit account, 마땅한 단어를 찾지 못해 직역함 - 역자 주) and 기초 계좌(originated accounts)의 차이는 무엇인가요? {#implicit_KR}

베이킹과 관련하여 고유 계정(implicit account)은 베이킹 프로세스(Baking process)에 참여할 수있는 계정입니다. 고유 계정은 다른 사람들이 위임 한 코인뿐만 아니라 자기가 보유한 코인으로도 (블록을) 생성 할 수(bake) 있습니다.

기초 계정(originated account)은 직접 블록을 생성 할 수 없지만 고유 계정(implicit account)에 코인을 위임 할 수 있습니다.
기초 계정(originated account)은 관리자 키(manager key)를 사용하여 대표 키(delegate key)를 지정함으로써 컨센서스 및 거버넌스에 대한 지분을 대표하는 대리인을 선택할 수 있습니다.

# 위임(delegate) 할 베이커를 어떻게 선택 할 수 있죠? {#bakerselection_KR}

먼저 [이 페이지에서](https://mytezosbaker.com/) 베이커들에 대해서 찾아 볼 수 있습니다. 아래와 같이 위임을 할 때 고려해야 할 몇가지 요소들이 있습니다:

1. **수수료**. 베이커가 요구하는 수수료는 얼마입니까?
2. **코인 수용량(Capacity)**. 각 베이커는 현재 보유하고 있는 코인의 수를 기준으로 환 코인수용량 (How many coins it can accept)이 있습니다. 베이커가 현재 보유하고있는 동전이라고 볼 수있는 위임량을 초과하면 "overdelegated"됩니다.
3. **신뢰도 + 고객대응도(Responsiveness)**. 이 베이커는 제 시간에 이익금을 지불합니까? 이 베이커는 정확하게 이익금을 지불합니까? 이 베이커는 제공하는 서비스에 관한 내 질문에 응답을 잘 하나요? 많은 베이커들이 대표자(Delegators)와 연락하기 위해 포럼 및 채팅을 운영합니다.
4. **보안** 이 베이커의 스테이킹 설정이 안전한가요? 이 베이커는 실적(Track record)이 있습니까? 이 베이커는 과거에 더블베이킹을 해서 코인을 잃었습니까?

# 베이킹 관련 자료들 {#resources_KR}

- [Tezos Ledger Nano S에서 Tezos 시작하기 (영문)](https://medium.com/@obsidian.systems/getting-started-with-tezos-on-the-ledger-nano-s-c011517b0f3c)
- [Tezos에서 혼자 베이킹(solo baking) 할 때 예상되는 수익(영문)](https://medium.com/cryptium/coquito-tezem-ergo-sum-expected-rewards-from-solo-baking-tezos-fcb4616b97dc)
- [위임하는 동안 (delegating) XTZ 를 Ledger Nano S에 보관하기(영문)](https://medium.com/cryptium/how-to-store-your-tezos-xtz-in-your-ledger-nano-s-and-delegate-with-tezbox-wallet-8fb4ac2d3355)
- [더블베이킹(Double Baking) --어떻게 베이커가 XTZ를 잃는가(영문)](https://medium.com/cryptium/half-baked-is-always-better-than-double-baked-what-is-at-stake-in-the-tezos-protocol-6619ce4a5f87)
- [Bakechain(영문)](https://bakechain.github.io/)