# 자가 개정(Self Amendment)

# 자가 개정 (Self Amendment)가 무엇인가요? {#introduction_KR}

Tezos는 하드 포크(hard fork)할 필요없이 프로토콜 업그레이드를 제안하고, 선택하며, 테스트 및 작동시키기 위한 온 체인(on-chain)매커니즘을 포함한 자체 개정 (기능을 가진) 블록체인 네트워크입니다.

간단히 말해서, 이것은 Tezos가 프로토콜 업그레이드를 위한 공식화 된 프로세스를 통해 시간이 지남에 따라 발전하는 블록 체인이라는 것을 의미합니다. 이는 실제로 주주가 회사의 방향에 투표하는 법인 구조와 유사합니다.

많은 다른 블록 체인에는 이러한 유형의 공식적인 거버넌스 구조가 없으므로 이러한 프로젝트의 방향은 소수의 개발자 그룹이나 모든 이해 관계자를 공정하게 대표 할수도 있고 아닐 수도 있는 재단에 의해 결정되기도합니다.

# 자가 개정은 어떻게 작동하나요? {#how_KR}

자가 개정 프로세스(Self amendment process)는 (프로토콜)프로포절 기간, 투표 기간,(프로포절)탐구 투표기간, (프로포절)테스트 기간 및 프로모션 투표 기간의 4가지 기간으로 나뉩니다. 이 4가지 기간이 각 기간동안 8 회의 베이킹 주기 (즉, 32,768 블록 또는 대략 22일, 18 시간)동안 지속되고 프로포절에서 작동까지 거의 정확히 3 개월이 걸립니다.

일정 기간 동안 어떤식으로든 진행에 실패 할 경우, 전체 프로세스가 프로포절 기간으로 되돌아가고 전체 프로세스를 사실상 다시 시작합니다.

![](../img/self-amendment.png)

## 1. 프로포절 기간

Tezos의 (자가) 개정 프로세스는 베이커들이 온-체인(on-chain)에서 프로포절을 제출할 수 있는 프로포절 기간부터 시작합니다. 베이커는 소스 코드의 해시를 제출하여 프로포절을 제출합니다.

각 프로포절 기간에 베이커는 최대 20건의 프로포절을 제출할 수 있습니다. 프로포절 제출은 투표로 계산되며, 이는 (프로포절) 기간 시작시에 지분(staking) 잔고의 롤 수와 같습니다. 프로포절 기간 동안 다른 베이커는 프로포절에 20번까지 투표 할 수 있습니다.

프로포절 기간이 끝날때, 네트워크는 프로포절 투표 수와 가장 많이 표를 받은 프로포절을 계속해서 (프로포절) 탐구 기간으로 진행합니다. 프로포절이 제출되지 않았거나 프로포절 사이에 동점이 있는 경우, 새로운 프로포절 기간이 시작됩니다.

## 2. (프로포절) 탐구 투표 기간

(프로포절) 탐사 투표 기간에 제빵업자는 이전 프로포절 기간에거 가장 표를 많이 받은 프로포절에 투표 할 수 있습니다. 베이커는 특정 프로포절에 대해 "찬성", "반대"또는 "기권"을 투표합니다. "기권"은 프로포절에 "투표하지 않는다"는 것을 의미합니다. 프로포절 기간과 마찬가지로, 베이커의 투표는 지분(Staking) 잔고의 롤 수에 기반합니다.

(프로포절)탐구 투표 기간이 끝나면 네트워크가 표를 집계합니다. 투표 참여 ( "찬성", "반대"및 "기권"의 합계)가 목표를 충족하고 기권하지 않은 베이커의 80%의 득표로 찬성하면, 프로포절은 테스트 기간으로 진행됩니다.

투표 참여 타켓은 과거 (투표)참여율의 지수 이동 평균(Exponetial Moving Average, EMA)과 매치시키려고 합니다. 투표 참여가 목표를 달성하지 못하거나 80%의 압도적 다수의 득표(supermajority)가 충족되지 않으면 수정 프로세스가 프로포절 기간으로 재시작됩니다

## 3. (프로포절)테스트 기간

프로포절이 (프로포절)탐구 투표 기간에 승인되면 48시간 동안 메인 네트워크와 같이 병렬로 실행되는 테스트넷 포크(testnet fork)로 테스트 기간이 시작됩니다. 이 포크는 표준 라이브러리에 액세스 할 수 있지만 샌드 박스화 되어 있습니다.

이 테스트 기간은 프로포절이 프로토콜을 개정할 만한 것인지에 대한 여부를 결정하는 데 사용됩니다. 테스트넷 포크 (testnet fork)는 업그레이드가 채택된다면, 블록체인 네트워크를 손상시키지 않고 네트워크가 유효한 상태 전이를 계속할 것이라는 것을 보장합니다.

## 4. 프로모션 투표 기간

(프로포절)테스트 기간이 끝나면 프로모션 투표 기간이 시작됩니다. 이 기간 동안 네트워크는 테스트 기간 동안 오프-체인(off-chain) 토론과 그 행동을 기반으로 해서 개정안 채택 여부를 결정합니다. (프로포절)탐구 투표 기간에서와 마찬가지로, 베이커는 투표 기능(ballot operation)을 사용하여 투표를 제출하며, 베이커의 투표는 자신의 지분(Staking) 잔고에 있는 롤 수에 비례하여 가중치가 적용됩니다.

프로모션 투표 기간이 끝나면 네트워크는 투표 수를 계산합니다. 참여율이 최소 정족수에 도달하고 기권하지 않은 베이커의 80%의 압도적 다수가 "찬성" 이라고 투표하면 프로포절이 새로운 메인넷으로 작동합니다. 그렇지 않으면 프로세스가 다시 한 번 프로포절 기간으로 돌아갑니다. 최소 투표 참여율은 과거 참여율을 기준으로 설정됩니다.

거버넌스 프로세스(Governance process)의 상세를 보려면 [이 링크](https://medium.com/tezos/amending-tezos-b77949d97e1e)를 참조하세요.

# 오프체인 가버넌스 (Off-chain governance) {#offchain_KR}

## Kialo

현재 일어나고 있는 Tezos 의 프로포절 논의에 대해선 [여기](https://www.kialo.com/tezos-protocol-amendment-1-25295/settings?back=%2Ftezos-protocol-amendment-1-25295%2F25295.0%3D25295.2%2B25295.81%2F%2B25295.81%2Fcomments)를 참고하세요.

## TezVote

[TezVote](TezVote.com)는 모든 Tezos 토큰 보유자가 온 체인(on-chain) 개정 프로포절에 대한 자신의 선호도를 알리거나 베이커가 어떻게 투표를 할것인지 확인 할 수 있습니다.

# 온-체인(on-chain) 거버넌스에 대한 반대와 논쟁들 {#arguments_KR}

온 체인 (on-chain) 거버넌스에 대한 다양한 논의와 다양한 관점을 이해하기 위해 아래와 같은 유용한 목록이 있습니다.

- [Blockchain Governance: Programming Our Future](https://medium.com/@FEhrsam/blockchain-governance-programming-our-future-c3bfe30f2d74) by Fred Ehrsam 
- [Against on-chain governance](https://medium.com/@Vlad_Zamfir/against-on-chain-governance-a4ceacd040ca) by Vlad Zamfir
- [Blockchains should not be democracies](https://hackernoon.com/blockchains-should-not-be-democracies-14379e0e23ad) by Haseeb Qureshi
- [Blockchains are not startups](https://medium.com/tezos/blockchains-are-not-startups-16449e210a61) by Jacob Arluck
- [Notes on Blockchain Governance](https://vitalik.ca/general/2017/12/17/voting.html) by Vitalik Buterin
- [Governance, Part 2: Plutocracy Is Still Bad](https://vitalik.ca/general/2018/03/28/plutocracy.html) by Vitalik Buterin
- [Debate on Blockchain Governance Podcast](https://www.zeroknowledge.fm/52) with Gavin Wood & Vlad Zamfir
- [Web3 Summit Governance Panel with Vlad Zamfir, Gavin Wood, Arthur Breitman & Adrian Brink](https://www.youtube.com/watch?v=eO3fG_1YrE4) with Gavin Wood, Vlad Zamfir, & Arthur Breitman
- [Conspiratus Podcast #1: Protocol Governance with Vlad Zamfir and Arthur Breitman](https://www.youtube.com/watch?v=IDY_inT-q0U) with Arthur Breitman, Vlad Zamfir, Sunny Aggrawal, & Nate Rush
- [Epicenter #259 Gavin Wood: Substrate, Polkadot and the Case for On-Chain Governance](https://www.youtube.com/watch?v=eP4mT19S_jg) with Gavin Wood